import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();
        String[] dateArray = date.split("-");
        String[] reorderedDate = new String[dateArray.length];

        reorderedDate[0] = dateArray[1] + "/";
        reorderedDate[1] = dateArray[2] + "/";
        reorderedDate[2] = dateArray[0];

        for(String item : reorderedDate) {
            System.out.print(item);
        }
    }
}