import java.util.*;

public class Main {
    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        String[] num = scanner.nextLine().split("");

        int firstSum = 0;
        int secondSum = 0;
        for(int i = 0; i < num.length; i++) {
            if(i < num.length / 2) {
                firstSum += Integer.parseInt(num[i]);
            }
            if(i >= num.length / 2) {
                secondSum += Integer.parseInt(num[i]);
            }
        }

        if(firstSum == secondSum) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}