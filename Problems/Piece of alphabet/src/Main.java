import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        char[] term = scanner.next().toCharArray();

        boolean isSequence = false;
        boolean isNotSequence = false;
        for(int i = 0; i < term.length -1; i++) {
            char letter = term[i];
            char next = letter += 1;

            if(next == term[i+1]) {
                isSequence = true;
            } else {
                isNotSequence = true;
            }
        }

        if(isNotSequence) {
            System.out.println(false);
        } else {
            System.out.println(true);
        }
    }
}