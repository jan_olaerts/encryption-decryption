package encryptdecrypt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static String mode = "enc";
    private static int key = 0;
    private static String message = "";
    private static String readFile = "";
    private static String writeFile = "";
    private static char[] messageArray;

    private static String algorithm = "shift";

    public static void main(String[] args) {

        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-mode")) {
                mode = args[i+1];
            }

            if(args[i].equals("-key")) {
                key = Integer.parseInt(args[i+1]);
            }

            if(args[i].equals("-in")) {
                readFile = args[i+1];
                message = readMessage(readFile);
                messageArray = message.toCharArray();
            }

            if(args[i].equals("-out")) {
                writeFile = args[i+1];
                writeMessage(writeFile);
            }

            if(args[i].equals("-data")) {
                message = args[i+1];
                messageArray = message.toCharArray();
            }

            if(args[i].equals("-alg")) {
                algorithm = args[i+1];
            }
        }

        String fileOrTerminal = writeFileOrTerminal(args);

        if(fileOrTerminal.equals("terminal") && mode.equals("enc") && algorithm.equals("unicode")) {
            encryptUnicode(messageArray, key);
            for(char item : messageArray) {
                System.out.print(item);
            }
        }

        if(fileOrTerminal.equals("file") && mode.equals("enc") && algorithm.equals("unicode")) {
            encryptUnicode(messageArray, key);
            writeMessage(writeFile);
        }

        if(fileOrTerminal.equals("terminal") && mode.equals("dec") && algorithm.equals("unicode")) {
            decryptUnicode(messageArray, key);
            for(char item : messageArray) {
                System.out.print(item);
            }
        }

        if(fileOrTerminal.equals("file") && mode.equals("dec") && algorithm.equals("unicode")) {
            decryptUnicode(messageArray, key);
            writeMessage(writeFile);
        }

        if(fileOrTerminal.equals("terminal") && mode.equals("enc") && algorithm.equals("shift")) {
            encryptShift(messageArray, key);
            for(char item : messageArray) {
                System.out.print(item);
            }
        }

        if(fileOrTerminal.equals("file") && mode.equals("enc") && algorithm.equals("shift")) {
            encryptShift(messageArray, key);
            writeMessage(writeFile);
        }

        if(fileOrTerminal.equals("terminal") && mode.equals("dec") && algorithm.equals("shift")) {
            decryptShift(messageArray, key);
            for(char item : messageArray) {
                System.out.print(item);
            }
        }

        if(fileOrTerminal.equals("file") && mode.equals("dec") && algorithm.equals("shift")) {
            decryptShift(messageArray, key);
            writeMessage(writeFile);
        }
    }

    public static String writeFileOrTerminal(String[] args) {
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-out")) {
                return "file";
            }
        }

        return "terminal";
    }

    public static void encryptUnicode(char[] messageArray, int shift) {
        for(int i = 0; i < messageArray.length; i++) {
            messageArray[i] += shift;
        }
    }

    public static void decryptUnicode(char[] messageArray, int shift) {
        for(int i = 0; i < messageArray.length; i++) {
            messageArray[i] -= shift;
        }
    }

    public static void encryptShift(char[] messageArray, int shift) {
        ArrayList<Integer> indexes = new ArrayList<>();

        for(int i = 0; i < messageArray.length; i++) {
            for(int j = 1; j <= shift; j++) {
                if(messageArray[i] >= 'a' && messageArray[i] <= 'z' && messageArray[i] + j > 'z' && messageArray[i] != ' ' && messageArray[i] != '!') {
                    int rest = shift - j;
                    char a = 'a';
                    messageArray[i] = a += rest;
                    indexes.add(i);
                }
                if(messageArray[i] >= 'A' && messageArray[i] <= 'Z' && messageArray[i] + j > 'Z' && messageArray[i] != ' ' && messageArray[i] != '!') {
                    int rest = shift - j;
                    char A = 'A';
                    messageArray[i] = A += rest;
                    indexes.add(i);
                }
            }
        }

        for(int i = 0; i < messageArray.length; i++) {
            if(messageArray[i] != ' ' && !indexes.contains(i) && messageArray[i] != '!') {
                messageArray[i] += shift;
            }
        }
    }

    public static void decryptShift(char[] messageArray, int shift) {
        ArrayList<Integer> indexes = new ArrayList<>();

        for(int i = 0; i < messageArray.length; i++) {
            for(int j = 1; j <= shift; j++) {
                if(messageArray[i] - j > 'Z' && messageArray[i] - j < 'a' && messageArray[i] != ' ' && messageArray[i] != '!') {
                    int rest = shift - j;
                    char z = 'z';
                    messageArray[i] = z -= rest;
                    indexes.add(i);
                }
                if(messageArray[i] - j < 'a' && messageArray[i] - j < 'A' && messageArray[i] != ' ' && messageArray[i] != '!') {
                    int rest = shift - j;
                    char Z = 'Z';
                    messageArray[i] = Z -= rest;
                    indexes.add(i);
                }
            }
        }

        for(int i = 0; i < messageArray.length; i++) {
            if(messageArray[i] != ' ' && !indexes.contains(i) && messageArray[i] != '!') {
                messageArray[i] -= shift;
            }
        }
    }

    public static String readMessage(String readFile) {
        File file = new File("./" + readFile);
        ArrayList<String> readMessage = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {
            while(scanner.hasNext()) {
                readMessage.add(scanner.next());
            }
        } catch (IOException e) {
            System.out.println("Error while reading file, " + e.getMessage());
        }

        char[] readMessageCharacterArray = readMessage.toString().toCharArray();
        readMessageCharacterArray[0] = '\u0000';
        readMessageCharacterArray[readMessageCharacterArray.length -1] = '\u0000';

//        return readMessageCharacterArray.toString();
        return readMessage.toString().replace('[', '\u0000').replace(']', '\u0000').replace(",", "").trim();
    }

    public static void writeMessage(String writeFile) {
        File file = new File("./" + writeFile);

        try (FileWriter writer = new FileWriter(file)) {
            for(char item : messageArray) {
                writer.write(item);
            }
        } catch(IOException e) {
            System.out.println("Error while writing file, " + e.getMessage());
        }
    }
}